var xhr = new XMLHttpRequest();
xhr.onreadystatechange = function() {
  if (xhr.readyState === 4) {
    var repos = JSON.parse(xhr.response);
    repos.values.forEach(function(item){
      var url = item.links.html.href;
      var link = '<li><i class="fa-li fa fa-angle-right"></i>' +
          '<a href="' + url + '">' + url + '</a></li>';

      var element = document.getElementById('container');
      element.insertAdjacentHTML('beforeend', link);
    });
  }
};
xhr.open('GET', 'https://bitbucket.org/api/2.0/users/jtate/repositories', true, 2);
xhr.send(null);
